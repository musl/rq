use std::collections::HashMap;
use std::fs::create_dir_all;
use std::fs::OpenOptions;
use std::path::Path;

use anyhow::{Context, Error, Result};
use clap::Parser;
use fake::{faker, Dummy, Fake, Faker};
use log::{debug, Level};
use serde::Serialize;
use serde_json::json;
use serde_json::value::Value;
use std::path::PathBuf;

#[derive(Debug, Dummy, Serialize)]
struct Directory {
    #[dummy(faker = "faker::lorem::en::Word()")]
    directory_name: String,

    #[dummy(faker = "(Faker, 5..10)")]
    companies: Vec<Company>,
}

#[derive(Debug, Dummy, Serialize)]
struct Company {
    #[dummy(faker = "faker::company::en::CompanyName()")]
    name: String,

    #[dummy(faker = "(Faker, 5..10)")]
    customers: Vec<Customer>,
}

#[derive(Debug, Dummy, Serialize)]
struct Customer {
    #[dummy(faker = "faker::name::en::Name()")]
    name: String,

    #[dummy(faker = "0..120")]
    age: u8,

    #[dummy(faker = "faker::address::en::Latitude()")]
    lat: String,

    #[dummy(faker = "faker::address::en::Longitude()")]
    lon: String,

    #[dummy(faker = "faker::address::en::ZipCode()")]
    zip_code: String,

    #[dummy(faker = "faker::creditcard::en::CreditCardNumber()")]
    credit_card_number: String,

    #[dummy(faker = "100..999")]
    cvn: u16,

    #[dummy(faker = "faker::lorem::en::Paragraphs(1..2)")]
    bio: Vec<String>,
}

#[derive(Clone, Debug, Parser)]
#[clap(author, about, long_about = None, version)]
struct Cli {
    /// Output directory
    #[clap(short = 'o', long, default_value = "test_data")]
    output_directory: PathBuf,

    /// Log output level
    #[clap(short = 'l', long, default_value = "warn")]
    log_level: Level,
}

fn json_docs<'s>() -> HashMap<&'s str, Value> {
    HashMap::from([
        (
            "lorem_ipsum.json",
            json!(faker::lorem::en::Words(100..200).fake::<Vec<String>>()),
        ),
        ("customer.json", json!(Faker.fake::<Customer>())),
        ("company.json", json!(Faker.fake::<Company>())),
        ("directory.json", json!(Faker.fake::<Directory>())),
        (
            "embedded.json",
            json!({ "json": format!("{}", json!(Faker.fake::<Directory>())) }),
        ),
    ])
}

fn generate_test_data<P>(p: P) -> Result<()>
where
    P: AsRef<Path>,
{
    for (file_name, doc) in json_docs() {
        let mut buf = p.as_ref().to_path_buf();
        buf.push(file_name);

        let full_path = buf
            .to_str()
            .ok_or_else(|| Error::msg(format!("Unable to convert path to UTF-8: {:?}", buf)))?;

        debug!("wrote: {}", full_path);

        let f = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(full_path)
            .with_context(|| format!("Unable to open for writing: {}", full_path))?;
        serde_json::to_writer_pretty(f, &doc)?;
    }
    Ok(())
}

fn main() -> Result<()> {
    let config = Cli::parse();

    env_logger::init();

    create_dir_all(&config.output_directory)?;
    generate_test_data(&config.output_directory)?;

    Ok(())
}
