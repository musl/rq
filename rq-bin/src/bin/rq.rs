use std::io::Write;

use anyhow::{Context, Result};
use atty::Stream;
use clap::Parser;
use log::LevelFilter;
use serde_json::value::Value;
use syntect::easy::HighlightLines;
use syntect::highlighting::{Style, ThemeSet};
use syntect::parsing::SyntaxSet;
use syntect::util::{as_24_bit_terminal_escaped, LinesWithEndings};

use rq::prelude::*;

/// The command line interface for `rq`.
#[derive(Clone, Debug, Parser)]
#[clap(author, about, long_about = None, version)]
struct Cli {
    /// Input path; '-' denotes stdin
    #[clap(short = 'i', long = "input-path", default_value = "-")]
    reader: Reader,

    /// Skips reading input. Causes -i to be ignored. This is useful for scripts that generate
    /// data but don't filter input, wrapping non-serialized data from the command line, testing,
    /// and probably more.
    #[clap(short = 'I', long = "skip-input")]
    skip_input: bool,

    /// Outuput path; '-' denotes stdout
    #[clap(short = 'f', long = "output-path", default_value = "-")]
    writer: Writer,

    /// Log output level
    #[clap(short = 'l', long, default_value = "warn")]
    log_level: LevelFilter,

    /// Output serialization format. Options: json, pretty_json, yaml. Defaults to pretty_json when
    /// on a tty, json (non-indented) when not on a tty (piped, non-interactive shells, etc).
    #[clap(short = 'o', long, default_value = "pretty_json")]
    output_format: Format,

    /// Supppress color output. When not running on a tty, color is already suppressed.
    #[clap(short = 'n', long)]
    no_color: bool,

    /// Optional script to manipulate data
    #[clap(default_value = ".")]
    script: String,
}

fn read(config: &Cli) -> Result<Value> {
    let path = config.reader.path();
    let v = serde_json::from_reader(config.reader.clone())
        .with_context(|| format!("Unable to parse JSON from: {:?}", path))?;
    Ok(v)
}

fn write_color(v: Value, config: &Cli) -> Result<()> {
    let mut writer = config.writer.clone();
    let ps = SyntaxSet::load_defaults_newlines();
    let ts = ThemeSet::load_defaults();
    let theme = "base16-eighties.dark";

    match config.output_format {
        Format::YAML => {
            let syn = ps.find_syntax_by_extension("yaml").unwrap();
            let mut h = HighlightLines::new(syn, &ts.themes[theme]);
            let s = serde_yaml::to_string(&v).with_context(|| "Unable to serialize to YAML")?;
            for line in LinesWithEndings::from(&s) {
                let ranges: Vec<(Style, &str)> = h.highlight(line, &ps);
                let escaped = as_24_bit_terminal_escaped(&ranges[..], false);
                write!(writer, "{}", escaped).with_context(|| "Unable to write YAML")?;
            }
        }
        Format::PrettyJSON => {
            let syn = ps.find_syntax_by_extension("json").unwrap();
            let mut h = HighlightLines::new(syn, &ts.themes[theme]);
            let s = serde_json::to_string_pretty(&v)
                .with_context(|| "Unable to serialize to pretty JSON")?;
            for line in LinesWithEndings::from(&s) {
                let ranges: Vec<(Style, &str)> = h.highlight(line, &ps);
                let escaped = as_24_bit_terminal_escaped(&ranges[..], false);
                write!(writer, "{}", escaped).with_context(|| "Unable to write JSON")?;
            }
        }
        Format::JSON => {
            let syn = ps.find_syntax_by_extension("json").unwrap();
            let mut h = HighlightLines::new(syn, &ts.themes[theme]);
            let s = serde_json::to_string(&v).with_context(|| "Unable to serialize to JSON")?;
            for line in LinesWithEndings::from(&s) {
                let ranges: Vec<(Style, &str)> = h.highlight(line, &ps);
                let escaped = as_24_bit_terminal_escaped(&ranges[..], false);
                write!(writer, "{}", escaped).with_context(|| "Unable to write JSON")?;
            }
        }
    };

    writeln!(writer, "\x1b[0m")?;

    Ok(())
}

fn write(v: Value, config: &Cli) -> Result<()> {
    let writer = config.writer.clone();

    match config.output_format {
        Format::YAML => {
            serde_yaml::to_writer(writer, &v).with_context(|| "Unable to write YAML")?;
        }
        Format::PrettyJSON => {
            serde_json::to_writer_pretty(writer, &v)
                .with_context(|| "Unable to write pretty JSON")?;
        }
        Format::JSON => {
            serde_json::to_writer(writer, &v).with_context(|| "Unable to write JSON")?;
        }
    };

    if atty::is(Stream::Stdout) {
        writeln!(config.writer.clone())?;
    }

    Ok(())
}

fn main() -> Result<()> {
    let config = Cli::parse();

    env_logger::Builder::new()
        .filter_level(config.log_level)
        .target(env_logger::fmt::Target::Stderr)
        .init();

    let mut value = if !config.skip_input {
        read(&config)?
    } else {
        Value::Null
    };

    parse(&config.script, &mut value).with_context(|| "Failed to run the provided script")?;

    if atty::is(Stream::Stdout) && !config.no_color {
        write_color(value, &config)
    } else {
        write(value, &config)
    }
}
