//! Generalized I/O: Reading

use std::fmt::Debug;
use std::fs::OpenOptions;
use std::io::{stdin, BufReader, Read};
use std::str::FromStr;

use anyhow::{Context, Error};

/// Provides a way to open either stdin or a given file for reading and handle them in the same
/// way.
pub struct Reader {
    path: String,
    reader: Box<dyn Read>,
}

impl Reader {
    /// Returns the path that was used to construct the Reader.
    pub fn path(&self) -> String {
        self.path.clone()
    }
}

impl FromStr for Reader {
    /// The associated error that can be returned from trying to open a path described by the given
    /// string.
    type Err = Error;

    // TODO this is hard to test. To use any::downcast_ref, a little more work needs to be done
    // in this module.
    /// # Example
    ///
    /// ```
    /// use std::str::FromStr;
    /// use rq::reader::Reader;
    ///
    /// let r = Reader::from_str("-").unwrap();
    /// assert_eq!("-".to_string(), r.path());
    /// ```
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "-" => Ok(Reader {
                path: s.to_string(),
                reader: Box::new(BufReader::new(stdin())),
            }),
            _ => {
                let f = OpenOptions::new()
                    .read(true)
                    .open(s)
                    .with_context(|| format!("Unable to open for reading: {}", s))?;
                Ok(Reader {
                    path: s.to_string(),
                    reader: Box::new(BufReader::new(f)),
                })
            }
        }
    }
}

impl std::io::Read for Reader {
    fn read(&mut self, b: &mut [u8]) -> Result<usize, std::io::Error> {
        self.reader.read(b)
    }
}

impl Debug for Reader {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        self.path.fmt(f)
    }
}

impl Clone for Reader {
    fn clone(&self) -> Self {
        // TODO: Is this `unwrap()` unsafe?
        Self::from_str(self.path.as_str()).unwrap()
    }
}
