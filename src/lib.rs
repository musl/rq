//! `rq` is a library to manipulate serialized data.

pub mod format;
pub mod prelude;
pub mod reader;
pub mod script;
pub mod writer;
