//! Generalized I/O: Writing

use std::fmt::{Arguments, Debug, Error as StdFmtError, Formatter};
use std::fs::OpenOptions;
use std::io::{stdout, BufWriter, Error as StdIoError, Write};
use std::str::FromStr;

use anyhow::{Context, Error, Result};

/// Provides a way to open either stdin or a given file for writing and handle them in the same
/// way.
pub struct Writer {
    path: String,
    writer: Box<dyn Write>,
}

impl Writer {
    /// Returns the path that was used to construct the Writer.
    pub fn path(&self) -> String {
        self.path.clone()
    }
}

impl FromStr for Writer {
    type Err = Error;

    // TODO this is hard to test. To use any::downcast_ref, a little more work needs to be done
    // in this module.
    /// # Example
    ///
    /// ```
    /// use std::str::FromStr;
    /// use rq::writer::Writer;
    ///
    /// let w = Writer::from_str("-").unwrap();
    /// assert_eq!("-".to_string(), w.path());
    /// ```
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "-" => Ok(Writer {
                path: s.to_string(),
                writer: Box::new(BufWriter::new(stdout())),
            }),
            _ => {
                let f = OpenOptions::new()
                    .write(true)
                    .truncate(true)
                    .open(s)
                    .with_context(|| format!("Unable to open for writing: {}", s))?;
                Ok(Writer {
                    path: s.to_string(),
                    writer: Box::new(BufWriter::new(f)),
                })
            }
        }
    }
}

impl Clone for Writer {
    fn clone(&self) -> Self {
        // TODO: this panics if there's an eror with the supplied path
        Self::from_str(self.path.as_str()).unwrap()
    }
}

impl Debug for Writer {
    fn fmt(&self, f: &mut Formatter) -> Result<(), StdFmtError> {
        self.path.fmt(f)
    }
}

impl Write for Writer {
    fn flush(&mut self) -> Result<(), StdIoError> {
        self.writer.flush()
    }

    fn write(&mut self, b: &[u8]) -> Result<usize, StdIoError> {
        self.writer.write(b)
    }

    fn write_fmt(&mut self, args: Arguments<'_>) -> Result<(), StdIoError> {
        self.writer.write_fmt(args)
    }
}
