//!
//! A library to execute `rq` scripts against arbitrary `serde_json::value::Value`s.
//!

use anyhow::Result;
use log::debug;
use pest::Parser;
use serde_json::value::Value;

pub mod ast;
use ast::builder::build;

pub mod script_parser;
use script_parser::{Rule, ScriptParser};

pub mod util;

/// Parse and execute a script with a given input `Value`.
///
/// # Example
///
/// ```
/// use rq::script::parse;
/// use serde_json::{json, Value};
///
/// let s = ".1";
/// let mut v = json!([1, 2, 3]);
///
/// parse(s, &mut v);
///
/// assert_eq!(Value::from(2), v.to_owned());
/// ```
pub fn parse<S>(s: S, v: &mut Value) -> Result<()>
where
    S: AsRef<str>,
{
    let s = s.as_ref();

    let pairs = ScriptParser::parse(Rule::script, s)?;
    debug!("{:#?}", pairs);

    let ast = build(s, pairs);
    debug!("\n{:#?}", ast);

    ast.execute(v)
}
