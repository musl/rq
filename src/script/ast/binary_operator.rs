//! Binary Operators

use anyhow::{bail, Error, Result};

use std::fmt::{Display, Error as StdFmtError, Formatter};
use std::str::FromStr;

/// SymbolError represents an error resolving an enum variant from a symbol
#[derive(Clone, Debug)]
pub struct SymbolError(String);

impl Display for SymbolError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), StdFmtError> {
        write!(f, "{}", self.0)
    }
}

/// Represents all binary operators that are defined for `rq` scripts
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum BinaryOperator {
    /// Addition
    Add,
    /// Division
    Divide,
    /// Modulus
    Modulus,
    /// Multiplication
    Multiply,
    /// Power
    Power,
    /// Subtraction
    Subtract,
    /// Logical And
    And,
    /// Logical Or
    Or,
    /// Logical Xor
    Xor,
    /// Pipe Operator
    Pipe,
}

impl FromStr for BinaryOperator {
    type Err = Error;

    /// Resolves operator symbols to their variants
    ///
    /// # Example
    /// ```
    /// use std::str::FromStr;
    /// use rq::script::ast::binary_operator::{BinaryOperator, SymbolError};
    ///
    /// assert_eq!(BinaryOperator::Add, BinaryOperator::from_str("+").unwrap());
    /// assert!(BinaryOperator::from_str("not-a-valid-symbol").unwrap_err().is::<SymbolError>());
    /// ```
    fn from_str(s: &str) -> Result<Self> {
        use BinaryOperator::*;

        match s {
            "+" => Ok(Add),
            "&&" => Ok(And),
            "/" => Ok(Divide),
            "%" => Ok(Modulus),
            "*" => Ok(Multiply),
            "||" => Ok(Or),
            "|" => Ok(Pipe),
            "**" => Ok(Power),
            "-" => Ok(Subtract),
            "^" => Ok(Xor),
            _ => bail!(SymbolError(format!("Unknown BinaryOperator {}", s))),
        }
    }
}
