//! Functions

use anyhow::{bail, Error, Result};
use rand::prelude::SliceRandom;
use serde_json::value::Value;

use std::fmt::Debug;
use std::str::from_utf8;

use super::super::util::name_for_value;
use super::debug_info::DebugInfo;
use super::AstNode;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Function {
    pub name: String,
    pub args: Vec<AstNode>,
}

impl Function {
    pub fn execute(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        match self.name.as_str() {
            "base64_decode" => self.base64_decode(di, v),
            "base64_encode" => self.base64_encode(di, v),
            "choose" => self.choose(di, v),
            "delete" => self.delete(di, v),
            "flatten" => self.flatten(di, v),
            "from_json" => self.from_json(di, v),
            "keys" => self.keys(di, v),
            "length" => self.length(di, v),
            "map" => self.map(di, v),
            "reverse" => self.reverse(di, v),
            "to_json" => self.to_json(di, v),
            "values" => self.values(di, v),
            _ => {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Function not found: {}", self.name)),
                ))
            }
        }
    }

    fn delete(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        // TODO: this case needs work. It probably shouldn't set the deleted value to Null but I'm
        // not sure what we can do here.
        if self.args.is_empty() {
            let p = v.pointer_mut("").unwrap();
            *p = Value::Null;
            return Ok(());
        }

        match v {
            Value::Array(_) => self.delete_from_array(di, v),
            Value::Object(_) => self.delete_from_object(di, v),
            _ => {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot delete from {}", name_for_value(v))),
                ))
            }
        }
    }

    fn delete_from_array(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        let mut argv = vec![];

        for arg in self.args.clone() {
            let mut w = v.clone();
            arg.execute(di, &mut w)?;

            if !w.is_number() {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot index array with {}", name_for_value(&w))),
                ));
            }

            argv.push(w.as_f64().unwrap() as usize);
        }

        for av in argv {
            let a = v.as_array_mut().unwrap();
            a.remove(av);
        }

        Ok(())
    }

    fn delete_from_object(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        let mut argv = vec![];

        for arg in self.args.clone() {
            let mut w = v.clone();
            arg.execute(di, &mut w)?;

            if !w.is_string() {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot index array with {}", name_for_value(&w))),
                ));
            }

            argv.push(w.as_str().unwrap().to_string());
        }

        for av in argv {
            let a = v.as_object_mut().unwrap();
            a.remove(&av);
        }
        Ok(())
    }

    fn keys(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        if !v.is_object() {
            bail!(di.wrap(
                self,
                Error::msg(format!(
                    "Cannot return a list of keys for {}",
                    name_for_value(v)
                )),
            ));
        }

        let p = v.pointer_mut("").unwrap();
        *p = p
            .as_object()
            .unwrap()
            .keys()
            .cloned()
            .collect::<Vec<_>>()
            .into();

        Ok(())
    }

    fn values(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        if !v.is_object() {
            bail!(di.wrap(
                self,
                Error::msg(format!(
                    "Cannot return a list of values for {}",
                    name_for_value(v)
                )),
            ));
        }

        let p = v.pointer_mut("").unwrap();
        *p = p
            .as_object()
            .unwrap()
            .values()
            .cloned()
            .collect::<Vec<_>>()
            .into();

        Ok(())
    }

    fn flatten(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        match v {
            Value::Array(a) => {
                let mut r: Vec<Value> = vec![];
                for e in a {
                    match e.as_array() {
                        Some(b) => {
                            for f in b {
                                r.push(f.to_owned());
                            }
                        }
                        None => {
                            r.push(e.to_owned());
                        }
                    }
                }

                let p = v.pointer_mut("").unwrap();
                *p = Value::Array(r);
            }
            _ => {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot flatten {}", name_for_value(v))),
                ))
            }
        };

        Ok(())
    }

    fn map(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        if self.args.len() != 1 {
            bail!(Error::msg("Missing argument to map"));
        }

        match v {
            Value::Array(a) => {
                for e in a.iter_mut() {
                    self.args[0].execute(di, e)?;
                }
            }
            _ => {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot map over {}", name_for_value(v))),
                ))
            }
        };

        Ok(())
    }

    fn base64_encode(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        if !v.is_string() {
            bail!(di.wrap(
                self,
                Error::msg(format!("Cannot base64_encode {}", name_for_value(v))),
            ));
        }

        let b64 = base64::encode(v.as_str().unwrap());
        let p = v.pointer_mut("").unwrap();
        *p = Value::String(b64);

        Ok(())
    }

    fn base64_decode(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        if !v.is_string() {
            bail!(di.wrap(
                self,
                Error::msg(format!("Cannot base64_decode {}", name_for_value(v))),
            ));
        }

        let bytes = base64::decode(v.as_str().unwrap())?;
        let s = from_utf8(&bytes)?.into();
        let p = v.pointer_mut("").unwrap();
        *p = Value::String(s);

        Ok(())
    }

    fn choose(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        let trng = &mut rand::thread_rng();

        match v.clone() {
            Value::String(s) => {
                let s = s
                    .chars()
                    .collect::<Vec<char>>()
                    .choose(trng)
                    .ok_or_else(|| Error::msg("Attempted to choose from an empty string"))?
                    .to_string();

                let p = v.pointer_mut("").unwrap();
                *p = Value::String(s);
            }
            Value::Array(a) => {
                let s = a
                    .choose(trng)
                    .ok_or_else(|| Error::msg("Attempted to choose from an empty array"))?;

                let p = v.pointer_mut("").unwrap();
                *p = s.clone();
            }
            _ => {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot choose from {}", name_for_value(v))),
                ))
            }
        };

        Ok(())
    }

    #[allow(clippy::wrong_self_convention)]
    fn from_json(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        if !v.is_string() {
            bail!(di.wrap(
                self,
                Error::msg(format!("Cannot decode JSON from {}", name_for_value(v))),
            ));
        }

        let p = v.pointer_mut("").unwrap();
        let s = p.as_str().unwrap();
        *p = serde_json::from_str(s).map_err(Error::new)?;

        Ok(())
    }

    #[allow(clippy::wrong_self_convention)]
    fn to_json(&self, _di: &DebugInfo, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();
        *p = serde_json::to_string(p)?.into();

        Ok(())
    }

    fn length(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();

        match p {
            Value::String(s) => {
                *p = s.chars().count().into();
            }
            Value::Array(a) => {
                *p = a.len().into();
            }
            _ => {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot take length of {}", name_for_value(v))),
                ))
            }
        };

        Ok(())
    }

    fn reverse(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();

        match p {
            Value::String(s) => {
                *p = s.chars().rev().collect::<String>().into();
            }
            Value::Array(a) => {
                let r = a.iter().rfold(vec![], |mut a, b| {
                    a.push(b.clone());
                    a
                });

                *p = Value::Array(r);
            }
            _ => {
                bail!(di.wrap(
                    self,
                    Error::msg(format!("Cannot reverse {}", name_for_value(v))),
                ))
            }
        };

        Ok(())
    }
}
