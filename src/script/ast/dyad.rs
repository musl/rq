//! Dyadic Operations

use anyhow::{Error, Result};
use serde_json::value::Value;

use std::fmt::Debug;

use super::super::util::name_for_value;
use super::binary_operator::BinaryOperator;
use super::debug_info::DebugInfo;
use super::AstNode;

/// Represents a dyadic operation - a tuple of left-hand value, binary operator, and right-hand
/// value - where values are resolved from expressions.
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Dyad {
    pub lhs: Box<AstNode>,
    pub op: BinaryOperator,
    pub rhs: Box<AstNode>,
}

impl Dyad {
    pub fn execute(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();

        match self.op {
            BinaryOperator::Pipe => {
                self.execute_pipe(di, p)?;
            }
            BinaryOperator::Add => {
                let (a, b) = self.resolve_f64_tuple(di, p)?;
                *p = (a + b).into();
            }
            BinaryOperator::And => {
                let (a, b) = self.resolve_bool_tuple(di, p)?;
                *p = (a && b).into();
            }
            BinaryOperator::Divide => {
                let (a, b) = self.resolve_f64_tuple(di, p)?;
                *p = (a / b).into();
            }
            BinaryOperator::Modulus => {
                let (a, b) = self.resolve_f64_tuple(di, p)?;
                *p = (a % b).into();
            }
            BinaryOperator::Multiply => {
                let (a, b) = self.resolve_f64_tuple(di, p)?;
                *p = (a * b).into();
            }
            BinaryOperator::Or => {
                let (a, b) = self.resolve_bool_tuple(di, p)?;
                *p = (a || b).into();
            }
            BinaryOperator::Power => {
                let (a, b) = self.resolve_f64_tuple(di, p)?;
                *p = (a.powf(b)).into();
            }
            BinaryOperator::Subtract => {
                let (a, b) = self.resolve_f64_tuple(di, p)?;
                *p = (a - b).into();
            }
            BinaryOperator::Xor => {
                let (a, b) = self.resolve_bool_tuple(di, p)?;
                *p = (a ^ b).into();
            }
        };

        Ok(())
    }

    fn execute_pipe(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        self.lhs.execute(di, v)?;
        self.rhs.execute(di, v)
    }

    fn resolve_f64_tuple(&self, di: &DebugInfo, v: &mut Value) -> Result<(f64, f64)> {
        let d = &mut v.clone();
        self.lhs.execute(di, d)?;
        let lhs = d.as_f64().ok_or_else(|| {
            di.wrap(
                self,
                Error::msg(format!(
                    "The result of evaluating the left-hand side was a {} but a Number was expected.",
                    name_for_value(v)
                )),
            )
        })?;

        let d = &mut v.clone();
        self.rhs.execute(di, d)?;
        let rhs = d.as_f64().ok_or_else(|| {
            di.wrap(
                self,
                Error::msg(format!(
                    "The result of evaluating the right-hand side was a {} but a Number was expected.",
                    name_for_value(v)
                )),
            )
        })?;

        Ok((lhs, rhs))
    }

    fn resolve_bool_tuple(&self, di: &DebugInfo, v: &mut Value) -> Result<(bool, bool)> {
        let d = &mut v.clone();
        self.lhs.execute(di, d)?;
        let lhs = d.as_bool().ok_or_else(|| {
            di.wrap(
                self,
                Error::msg(format!(
                    "The result of evaluating the left-hand side was a {} but a Bool was expected.",
                    name_for_value(v)
                )),
            )
        })?;

        let d = &mut v.clone();
        self.rhs.execute(di, d)?;
        let rhs = d.as_bool().ok_or_else(|| {
            di.wrap(
                self,
                Error::msg(format!(
                    "The result of evaluating the right-hand side was a {} but a Bool was expected.",
                    name_for_value(v)
                )),
            )
        })?;

        Ok((lhs, rhs))
    }
}
