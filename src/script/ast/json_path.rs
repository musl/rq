//! JSON pathing

use anyhow::Result;
use serde_json::Value;

use super::debug_info::DebugInfo;
use super::json_index::JsonIndex;

/// Represents a collection of indices
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct JsonPath(Vec<JsonIndex>);

impl Default for JsonPath {
    /// Creates an empty path
    ///
    /// # Example
    /// ```
    /// use rq::script::ast::json_path::JsonPath;
    ///
    /// assert_eq!(JsonPath::new(vec![]), JsonPath::default());
    /// ```
    fn default() -> Self {
        Self::new(vec![])
    }
}

impl JsonPath {
    /// Returns an empty path
    pub fn new(v: Vec<JsonIndex>) -> Self {
        Self(v)
    }

    /// Adds an index to this path
    ///
    /// # Example
    /// ```
    /// use rq::script::ast::json_path::JsonPath;
    /// use rq::script::ast::json_index::JsonIndex;
    ///
    /// let mut jp = JsonPath::default();
    /// jp.push(JsonIndex::Dot);
    ///
    /// assert_eq!(1, jp.len());
    /// ```
    pub fn push(&mut self, index: JsonIndex) {
        self.0.push(index);
    }

    /// Returns the number of indices in this path
    pub fn len(&mut self) -> usize {
        self.0.len()
    }

    /// Returns the number of indices in this path
    pub fn is_empty(&mut self) -> bool {
        self.0.len() == 0
    }

    /// Sets the given value to the value yielded by traversing the given path
    pub fn execute(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        for index in self.0.iter() {
            index.execute(di, v).map_err(|e| di.wrap(self, e))?;
        }

        Ok(())
    }
}
