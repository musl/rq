//! AST Generation

use ordered_float::OrderedFloat;
use pest::iterators::{Pair, Pairs};
use pest::prec_climber::{Assoc, Operator, PrecClimber};

use std::str::FromStr;

use super::super::script_parser::Rule;
use super::binary_operator::BinaryOperator;
use super::json_index::JsonIndex;
use super::unary_prefix_operator::UnaryPrefixOperator;
use super::{Ast, AstNode, AstValue, DebugInfo, Dyad, Function, JsonPath, Monad};

struct Builder {
    debug_info: DebugInfo,
}

/// Build an AST from an iterator of `pest::iterators::Pairs<Rule>`.
///
/// # Example
///
/// ```
/// use pest::Parser;
/// use serde_json::{json, Value};
///
/// use rq::script::script_parser::{Rule, ScriptParser};
/// use rq::script::ast::builder::build;
///
/// let s = ".1";
/// let p = ScriptParser::parse(Rule::script, s).unwrap();
/// let ast = build(s, p);
/// let mut v = json!([1, 2, 3]);
///
/// ast.execute(&mut v);
///
/// assert_eq!(Value::from(2), v.to_owned());
/// ```
pub fn build<S>(s: S, mut pairs: Pairs<Rule>) -> Ast
where
    S: AsRef<str>,
{
    let mut b = Builder {
        debug_info: DebugInfo::new(s),
    };

    let root = b.build_from_expression(pairs.next().unwrap());
    let debug_info = b.debug_info;

    Ast { debug_info, root }
}

impl Builder {
    fn build_from_expression(&mut self, pair: Pair<Rule>) -> AstNode {
        let pair = pair.into_inner().next().unwrap();
        let span = pair.as_span();

        let n = match pair.as_rule() {
            Rule::dyad => self.build_from_dyad(pair),
            Rule::expression => self.build_from_expression(pair),
            Rule::function => self.build_from_function(pair),
            Rule::monad => self.build_from_monad(pair),
            Rule::path => self.build_from_path(pair),
            Rule::value => self.build_from_value(pair),
            _ => unreachable!(),
        };

        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_function(&mut self, pair: Pair<Rule>) -> AstNode {
        let mut inner = pair.into_inner();
        let name = inner.next().unwrap().as_str().to_string();
        let args = match inner.next() {
            Some(a) => a
                .into_inner()
                .map(|v| self.build_from_expression(v))
                .collect(),
            None => vec![],
        };

        AstNode::Function(Function { name, args })
    }

    fn build_from_path(&mut self, pair: Pair<Rule>) -> AstNode {
        let mut p = JsonPath::default();
        let span = pair.as_span();
        let inner = pair.into_inner();

        for pair in inner {
            let path = match pair.as_rule() {
                Rule::deconstructor => JsonIndex::Deconstructor,
                Rule::dot => JsonIndex::Dot,
                Rule::expression => self.build_index_from_expression(pair),
                Rule::ident => self.build_index_from_ident(pair),
                Rule::ordinal => self.build_index_from_ordinal(pair),
                Rule::slice => self.build_index_from_slice(pair),
                Rule::string => self.build_index_from_string(pair),
                _ => unreachable!(),
            };
            p.push(path);
        }

        let n = AstNode::JsonPath(p);
        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_index_from_expression(&mut self, pair: Pair<Rule>) -> JsonIndex {
        JsonIndex::Expression(Box::new(self.build_from_expression(pair)))
    }

    fn build_index_from_slice(&mut self, pair: Pair<Rule>) -> JsonIndex {
        let mut inner = pair.into_inner();
        let a = inner.next().unwrap().as_str().parse::<usize>().unwrap();
        let b = inner.next().unwrap().as_str().parse::<usize>().unwrap();

        JsonIndex::Slice((a, b))
    }

    fn build_index_from_ident(&mut self, pair: Pair<Rule>) -> JsonIndex {
        JsonIndex::Ident(pair.as_str().to_string())
    }

    fn build_index_from_string(&mut self, pair: Pair<Rule>) -> JsonIndex {
        JsonIndex::String(pair.into_inner().next().unwrap().as_str().to_string())
    }

    fn build_index_from_ordinal(&mut self, pair: Pair<Rule>) -> JsonIndex {
        JsonIndex::Ordinal(pair.as_str().parse::<usize>().unwrap())
    }

    fn build_from_primary(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let n = match pair.as_rule() {
            Rule::monad => self.build_from_monad(pair),
            Rule::value => self.build_from_value(pair),
            Rule::path => self.build_from_path(pair),
            Rule::function => self.build_from_function(pair),
            _ => unreachable!(),
        };

        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_monad(&mut self, pair: Pair<Rule>) -> AstNode {
        let mut inner = pair.into_inner();
        let op = UnaryPrefixOperator::from_str(inner.next().unwrap().as_str()).unwrap();
        let rhs = Box::new(self.build_from_primary(inner.next().unwrap()));

        AstNode::Monad(Monad { op, rhs })
    }

    fn build_from_dyad(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let inner = pair.into_inner();

        let pc = PrecClimber::new(vec![
            Operator::new(Rule::pipe, Assoc::Left),
            Operator::new(Rule::and, Assoc::Left),
            Operator::new(Rule::or, Assoc::Left),
            Operator::new(Rule::add, Assoc::Left) | Operator::new(Rule::subtract, Assoc::Left),
            Operator::new(Rule::multiply, Assoc::Left)
                | Operator::new(Rule::divide, Assoc::Left)
                | Operator::new(Rule::modulus, Assoc::Left),
            Operator::new(Rule::power, Assoc::Right),
        ]);

        let infix = |ln: AstNode, rule: Pair<Rule>, rn: AstNode| {
            let lhs = Box::new(ln);
            let rhs = Box::new(rn);
            let op = BinaryOperator::from_str(rule.as_str()).unwrap();

            AstNode::Dyad(Dyad { lhs, op, rhs })
        };

        let n = pc.climb(inner, |v| self.build_from_primary(v), infix);
        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_value(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let pair = pair.into_inner().next().unwrap();

        let n = match pair.as_rule() {
            Rule::array => self.build_from_array(pair),
            Rule::boolean => self.build_from_boolean(pair),
            Rule::null => AstNode::Value(AstValue::Null),
            Rule::number => self.build_from_number(pair),
            Rule::object => self.build_from_object(pair),
            Rule::string => self.build_from_string(pair),
            _ => unreachable!(),
        };

        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_array(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let inner = pair.into_inner();
        let elements = inner.map(|v| self.build_from_expression(v)).collect();

        let n = AstNode::Value(AstValue::Array(elements));
        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_boolean(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let n = AstNode::Value(AstValue::Boolean(pair.as_str().parse::<bool>().unwrap()));
        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_number(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let n = AstNode::Value(AstValue::Number(OrderedFloat(
            pair.as_str().parse::<f64>().unwrap(),
        )));
        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_object(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let inner = pair.into_inner();
        let elements = inner.map(|v| self.build_from_expression(v)).collect();

        let n = AstNode::Value(AstValue::Object(elements));
        self.debug_info.insert(n.clone(), span);
        n
    }

    fn build_from_string(&mut self, pair: Pair<Rule>) -> AstNode {
        let span = pair.as_span();
        let n = AstNode::Value(AstValue::String(
            pair.into_inner().next().unwrap().as_str().to_string(),
        ));
        self.debug_info.insert(n.clone(), span);
        n
    }
}
