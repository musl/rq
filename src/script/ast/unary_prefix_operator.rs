//! Unary Prefix Operators

use anyhow::{bail, Error, Result};

use std::fmt::{Display, Error as StdFmtError, Formatter};
use std::str::FromStr;

/// SymbolError represents an error resolving an enum variant from a symbol
#[derive(Clone, Debug)]
pub struct SymbolError(String);

impl Display for SymbolError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), StdFmtError> {
        write!(f, "{}", self.0)
    }
}

/// Represents all unary prefix operators that are defined for `rq` scripts
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum UnaryPrefixOperator {
    Negate,
    Not,
}

impl FromStr for UnaryPrefixOperator {
    type Err = Error;

    /// Resolves operator symbols to their variants
    ///
    /// # Example
    /// ```
    /// use std::str::FromStr;
    /// use rq::script::ast::unary_prefix_operator::{UnaryPrefixOperator, SymbolError};
    ///
    /// assert_eq!(UnaryPrefixOperator::Not, UnaryPrefixOperator::from_str("!").unwrap());
    /// assert!(UnaryPrefixOperator::from_str("not-a-valid-symbol").unwrap_err().is::<SymbolError>());
    /// ```
    fn from_str(s: &str) -> Result<Self> {
        use UnaryPrefixOperator::*;
        match s {
            "-" => Ok(Negate),
            "!" => Ok(Not),
            _ => bail!(SymbolError(format!("Unknown UnaryPrefixOperator {}", s))),
        }
    }
}
