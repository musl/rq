//! An AST for `rq` scripts

use anyhow::Result;
use itertools::Itertools;
use serde_json::value::Value;

use std::fmt::Debug;

pub mod builder;

pub mod binary_operator;

pub mod debug_info;
use debug_info::DebugInfo;

pub mod dyad;
use dyad::Dyad;

pub mod function;
use function::Function;

pub mod json_index;

pub mod json_path;
use json_path::JsonPath;

pub mod monad;
use monad::Monad;

pub mod unary_prefix_operator;

pub mod value;
use value::AstValue;

/// Represents any valid AST node.
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum AstNode {
    Dyad(Dyad),
    Expression(Box<AstNode>),
    Function(Function),
    JsonPath(JsonPath),
    Monad(Monad),
    Value(AstValue),
}

/// A struct that holds a root node of an AST and related debugging information.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Ast {
    debug_info: DebugInfo,
    root: AstNode,
}

impl Ast {
    /// Execute the script represented by this `Ast` with the given input `Value`.
    pub fn execute(&self, v: &mut Value) -> Result<()> {
        self.root.execute(&self.debug_info, v)
    }
}

impl AstNode {
    fn execute(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        match self {
            AstNode::Dyad(d) => d.execute(di, v)?,
            AstNode::Expression(e) => e.execute(di, v)?,
            AstNode::Function(f) => f.execute(di, v)?,
            AstNode::JsonPath(jp) => jp.execute(di, v)?,
            AstNode::Monad(m) => m.execute(di, v)?,
            AstNode::Value(av) => av.execute(di, v)?,
        };

        Ok(())
    }
}

impl FromIterator<AstNode> for Vec<(AstNode, AstNode)> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = AstNode>,
    {
        let mut v = vec![];

        for (a, b) in iter.into_iter().tuples() {
            v.push((a, b));
        }

        v
    }
}

impl From<&AstNode> for AstNode {
    fn from(an: &AstNode) -> Self {
        an.clone()
    }
}

impl From<&AstValue> for AstNode {
    fn from(av: &AstValue) -> Self {
        AstNode::Value(av.clone())
    }
}

impl From<&Dyad> for AstNode {
    fn from(d: &Dyad) -> Self {
        AstNode::Dyad(d.clone())
    }
}

impl From<&Function> for AstNode {
    fn from(f: &Function) -> Self {
        AstNode::Function(f.clone())
    }
}

impl From<&JsonPath> for AstNode {
    fn from(jp: &JsonPath) -> Self {
        AstNode::JsonPath(jp.clone())
    }
}

impl From<&Monad> for AstNode {
    fn from(m: &Monad) -> Self {
        AstNode::Monad(m.clone())
    }
}
