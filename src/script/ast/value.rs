//! Literals, Values, and Constructs

use anyhow::{bail, Error, Result};
use ordered_float::OrderedFloat;
use serde_json::value::Value;
use serde_json::Map;

use super::super::util::name_for_value;
use super::debug_info::DebugInfo;
use super::AstNode;

/// Represents literals, values, array, and objects that can be constructed
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum AstValue {
    Array(Vec<AstNode>),
    Boolean(bool),
    Null,
    Number(OrderedFloat<f64>),
    Object(Vec<(AstNode, AstNode)>),
    String(String),
}

impl AstValue {
    /// Sets the passed in value to the value resolved by interpreting the given literal,
    /// recursively constructing arrays or objects, or by evaluating the given expression.
    pub fn execute(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        match self {
            AstValue::Array(a) => self.resolve_array(a, di, v)?,
            AstValue::Boolean(b) => self.resolve_boolean(b, v)?,
            AstValue::Null => self.set(v, Value::Null)?,
            AstValue::Number(n) => self.resolve_number(n, v)?,
            AstValue::Object(o) => self.resolve_object(o, di, v)?,
            AstValue::String(s) => self.resolve_string(s, v)?,
        };

        Ok(())
    }

    fn set(&self, v: &mut Value, w: Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();
        *p = w;
        Ok(())
    }

    fn resolve_array(&self, a: &[AstNode], di: &DebugInfo, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();

        let mut r = vec![];

        for e in a.iter() {
            let w = &mut p.clone();
            e.execute(di, w)?;
            r.push(w.clone());
        }

        *p = Value::Array(r.to_vec());

        Ok(())
    }

    fn resolve_boolean(&self, b: &bool, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();
        *p = Value::from(*b);

        Ok(())
    }

    fn resolve_number(&self, n: &OrderedFloat<f64>, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();
        *p = Value::from(n.into_inner());

        Ok(())
    }

    fn resolve_object(
        &self,
        o: &[(AstNode, AstNode)],
        di: &DebugInfo,
        v: &mut Value,
    ) -> Result<()> {
        let p = v.pointer_mut("").unwrap();
        let mut m = Map::new();

        for (key_node, value_node) in o.iter() {
            let key_value = &mut p.clone();
            key_node.execute(di, key_value)?;

            if !key_value.is_string() {
                bail!(di.wrap(
                    &*key_node,
                    Error::msg(format!(
                        "Object key must be a String, found {}",
                        name_for_value(key_value)
                    )),
                ));
            }

            let key = key_value.as_str().unwrap().to_string();

            let value = &mut p.clone();
            value_node.execute(di, value)?;

            m.insert(key, value.clone());
        }

        *p = Value::Object(m);

        Ok(())
    }

    fn resolve_string(&self, s: &str, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();
        *p = Value::from(s.to_string());

        Ok(())
    }
}
