//! Monadic operations

use anyhow::{Error, Result};
use serde_json::value::Value;

use std::fmt::Debug;

use super::super::util::name_for_value;
use super::debug_info::DebugInfo;
use super::unary_prefix_operator::UnaryPrefixOperator;
use super::AstNode;

/// Represents a monadic operation - a tuple of a unary prefix operator and a value resolved from
/// an expression.
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Monad {
    pub op: UnaryPrefixOperator,
    pub rhs: Box<AstNode>,
}

impl Monad {
    /// Sets the passed value to the result of the operation
    pub fn execute(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();

        *p = match self.op {
            UnaryPrefixOperator::Negate => (self.resolve_f64(di, p)? * -1.0).into(),
            UnaryPrefixOperator::Not => (!self.resolve_bool(di, p)?).into(),
        };

        Ok(())
    }

    fn resolve_f64(&self, di: &DebugInfo, v: &mut Value) -> Result<f64> {
        self.rhs.execute(di, v)?;
        v.as_f64().ok_or_else(|| {
            di.wrap(
                self,
                Error::msg(format!(
                    "The result of evaluating the operand was a {}, but a Number was expected.",
                    name_for_value(v)
                )),
            )
        })
    }

    fn resolve_bool(&self, di: &DebugInfo, v: &mut Value) -> Result<bool> {
        self.rhs.execute(di, v)?;
        v.as_bool().ok_or_else(|| {
            di.wrap(
                self,
                Error::msg(format!(
                    "The result of evaluating the operand was a {}, but a Bool was expected.",
                    name_for_value(v)
                )),
            )
        })
    }
}
