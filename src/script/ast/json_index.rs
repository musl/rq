//! Index Operations

use anyhow::{bail, Error, Result};
use serde_json::Value;

use std::fmt::Error as StdFmtError;
use std::fmt::{Display, Formatter};

use super::super::util::name_for_value;
use super::debug_info::DebugInfo;
use super::AstNode;

/// Represents all the ways a value may be indexed
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum JsonIndex {
    /// The identity operator
    Dot,
    /// Deconstructs objects into lists of values
    Deconstructor,
    /// Allows values to be indexed by the result of an expression
    Expression(Box<AstNode>),
    /// Bare-string indexing for simple keys (`.name`)
    Ident(String),
    /// Full string indexes inside square brackets (`["name"]`)
    String(String),
    /// Both array index short-hand (`.0`) and square bracket indexing: (`[0]`)
    Ordinal(usize),
    /// Array slicing (`[a:b]`), this has the same behavior as rust slicing
    Slice((usize, usize)),
}

impl Display for JsonIndex {
    fn fmt(&self, f: &mut Formatter) -> Result<(), StdFmtError> {
        match self {
            Self::Dot => write!(f, "."),
            Self::Deconstructor => write!(f, ".[]"),
            Self::Expression(_n) => write!(f, "expression"),
            Self::Ident(i) => write!(f, "{}", i),
            Self::String(i) => write!(f, "{}", i),
            Self::Ordinal(o) => write!(f, "{}", o),
            Self::Slice((a, b)) => write!(f, "{}..{}", a, b),
        }
    }
}

impl JsonIndex {
    /// Sets the passed-in value to the result of performing the given index
    pub fn execute(&self, di: &DebugInfo, v: &mut Value) -> Result<()> {
        match self {
            JsonIndex::Dot => {}
            JsonIndex::Deconstructor => self.deconstruct(v)?,
            JsonIndex::Expression(e) => e.execute(di, v)?,
            JsonIndex::Ident(i) => self.index_object_by_ident(i, v)?,
            JsonIndex::Ordinal(o) => self.index_array_by_ordinal(o, v)?,
            JsonIndex::Slice(t) => self.slice_array(t, v)?,
            JsonIndex::String(i) => self.index_object_by_string(i, v)?,
        };

        Ok(())
    }

    fn deconstruct(&self, v: &mut Value) -> Result<()> {
        match v {
            Value::Array(_) => self.deconstruct_array(v)?,
            Value::Object(_) => self.deconstruct_object(v)?,
            _ => {
                bail!(Error::msg(format!(
                    "Cannot deconstruct {}",
                    name_for_value(v)
                )));
            }
        }

        Ok(())
    }

    fn deconstruct_array(&self, _v: &mut Value) -> Result<()> {
        // TODO: Right now, this is a no-op. At some point, we might support non-value lists, but
        // I'm not yet sure yet why we would need to.

        Ok(())
    }

    fn deconstruct_object(&self, v: &mut Value) -> Result<()> {
        let p = v.pointer_mut("").unwrap();

        *p = Value::Array(p.as_object().unwrap().values().cloned().collect());

        Ok(())
    }

    fn index_object_by_ident(&self, i: &str, v: &mut Value) -> Result<()> {
        if !v.is_object() {
            bail!(Error::msg(format!(
                "Cannot index {} with String: '{}'",
                name_for_value(v),
                i
            )));
        }

        let p = v.pointer_mut("").unwrap();
        *p = p.get(i).cloned().unwrap_or(Value::Null);

        Ok(())
    }

    fn index_array_by_ordinal(&self, o: &usize, v: &mut Value) -> Result<()> {
        if !v.is_array() {
            bail!(Error::msg(format!(
                "Cannot index {} with Number: {}",
                name_for_value(v),
                o
            )));
        }

        let p = v.pointer_mut("").unwrap();
        *p = p.get(o).cloned().unwrap_or(Value::Null);

        Ok(())
    }

    fn slice_array(&self, t: &(usize, usize), v: &mut Value) -> Result<()> {
        let (a, b) = t;

        if !v.is_array() {
            bail!(Error::msg(format!("Cannot slice {}", name_for_value(v))));
        }

        if a > b {
            bail!(Error::msg(format!(
                "The end of a slice index must be greater than its start: [{}:{}]",
                a, b
            )));
        }

        let p = v.pointer_mut("").unwrap();
        *p = Value::Array(p.as_array().unwrap()[*a..*b].to_vec());

        Ok(())
    }

    fn index_object_by_string(&self, i: &str, v: &mut Value) -> Result<()> {
        if !v.is_object() {
            bail!(Error::msg(format!(
                "Cannot index {} with String: '{}'",
                name_for_value(v),
                i
            )));
        }

        let p = v.pointer_mut("").unwrap();
        *p = p.get(i).cloned().unwrap_or(Value::Null);

        Ok(())
    }
}
