//! Debug Information and Error Wrapping

use anyhow::Error;
use pest::Span;

use std::collections::HashMap;
use std::fmt::{Debug, Display};
use std::marker::{Send, Sync};

use super::AstNode;

type SpanMap = HashMap<AstNode, (usize, usize)>;

/// A copy of the script to be run and a map of span locations and AstNodes for decorating errors.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DebugInfo {
    pub map: SpanMap,
    pub script: String,
}

impl<'n> DebugInfo {
    pub fn new<S>(s: S) -> Self
    where
        S: AsRef<str>,
    {
        let map = HashMap::new();
        let script = s.as_ref().to_string();
        Self { map, script }
    }

    pub fn insert(&mut self, node: AstNode, span: Span) {
        self.map.insert(node, (span.start(), span.end()));
    }

    pub fn err<S, N: 'n>(&self, node: &'n N, s: S) -> Error
    where
        S: Display + Debug + Send + Sync + 'static,
        AstNode: From<&'n N>,
    {
        self.wrap(node, Error::msg(s))
    }

    pub fn wrap<N: 'n>(&self, node: &'n N, err: Error) -> Error
    where
        AstNode: From<&'n N>,
    {
        let node = node.into();
        let t = self.map.get(&node);
        if t.is_none() {
            return Error::msg(format!("Unable to find span for node {:?}", node));
        }
        let (start, end) = t.unwrap();
        let indicator = (0..*end)
            .into_iter()
            .map(|n| if n < *start { ' ' } else { '^' })
            .collect::<String>();

        err.context(format!(
            "Script Location:\n\n\t{}\n\t{}\n",
            self.script, indicator
        ))
    }
}
