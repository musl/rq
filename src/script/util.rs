//! Utility code. A junk drawer.

use serde_json::value::Value;

/// Return the name of a `serde_json::value::Value` as a string.
pub fn name_for_value(v: &Value) -> &'static str {
    // TODO: It really feels like there should be a built-in way to do this.
    match v {
        Value::Null => "Null",
        Value::Bool(_) => "Bool",
        Value::Number(_) => "Number",
        Value::String(_) => "String",
        Value::Array(_) => "Array",
        Value::Object(_) => "Object",
    }
}
