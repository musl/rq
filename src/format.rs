//! Serialization Formats

use anyhow::{bail, Error, Result};
use std::fmt::{Display, Error as StdFmtError, Formatter};
use std::str::FromStr;

/// `rq` can output data in a few formats. Each format is reperesented by a variant of this `enum`.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Format {
    /// Un-indented JSON.
    JSON,
    /// Indented, "pretty" JSON.
    PrettyJSON,
    /// It's YAML.
    YAML,
}

/// FormatError is returned when an unknown format name is encountered.
#[derive(Clone, Debug)]
pub struct FormatError(String);

impl Display for FormatError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), StdFmtError> {
        write!(f, "{}", self.0)
    }
}

impl FromStr for Format {
    type Err = Error;

    /// # Examples
    ///
    /// ```
    /// use std::str::FromStr;
    /// use rq::format::{Format, FormatError};
    ///
    /// assert_eq!(Format::JSON, Format::from_str("json").unwrap());
    /// assert_eq!(Format::PrettyJSON, Format::from_str("pretty_json").unwrap());
    /// assert_eq!(Format::YAML, Format::from_str("yaml").unwrap());
    /// assert!(Format::from_str("not-a-valid-format").unwrap_err().is::<FormatError>());
    /// ```
    ///
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "json" => Ok(Format::JSON),
            "pretty_json" => Ok(Format::PrettyJSON),
            "yaml" => Ok(Format::YAML),
            _ => bail!(FormatError(format!("Unknown format: {}", s))),
        }
    }
}
